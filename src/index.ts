import * as _ from 'lodash';
import * as R from 'ramda';
import * as P from 'bluebird';

import {
    readdir,
    readFile,
    writeFile
} from 'fs';
const [
    readdirAsync,
    readFileAsync,
    writeFileAsync
] = [
    readdir,
    readFile,
    writeFile
].map(P.promisify);

import {
    createClient,
    createServer
} from 'http';